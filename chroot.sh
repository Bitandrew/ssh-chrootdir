#!/usr/bin/env bash

# Create chroot directory.

DEFDIR=chroot
RPMGOOGLE="google-authenticator-1.01-1.el7.centos.x86_64.rpm"
SSHCONF=/etc/ssh/sshd_config
PAMCONF=/etc/pam.d/sshd
array=(etc home var usr tmp dev lib64 bin)


chroot_env() {
echo ""
echo "By default will be created $DEFDIR"
echo ""
        echo  "" 
        read -p "Enter new name for chroot directory or press ENTER : " CHROOT

        if [ "$CHROOT" = "" ] ; then
           echo "The Directory stayed by default $DEFDIR"
           ${CHROOT:=$DEFDIR}
        fi
        
        if  [ ! -d /$CHROOT ] 
           then
                   echo "Create Dir /$CHROOT and ${array[@]}"
                   mkdir -p /${CHROOT}/{etc,var,dev,home,lib64,bin,tmp,usr/{lib64,bin,sbin,local}}
                   ls -1R /${CHROOT}  | sed '/^$/d'
     		  		
  					chmod go-w 	/${CHROOT}
      					chmod 777 	/${CHROOT}/tmp
      					mknod -m 666 	/${CHROOT}/dev/tty c 5 0
					mknod -m 666 	/${CHROOT}/dev/null c 1 3
					mknod -m 444 	/${CHROOT}/dev/random c 1 8
					mknod -m 444 	/${CHROOT}/dev/urandom c 1 9
					chown root:tty 	/${CHROOT}/dev/tty
				echo "The ${CHROOT} created "
           else
               echo "The directory is already used."

        fi
	echo ""
        
}


install_rpm() {
echo "If chroot dir is installed, Input chrooot dir name or press Enter"
read chroot_dir


http_code=`curl -Is http://yum-repo01/Magento/google-authenticator-1.01-1.el7.centos.x86_64.rpm |grep '\([2]0[0-9]\).*OK' | awk '{print $2}'`
if [ "$http_code" -eq 200 ];then 
    echo " Install package ${RPMGOOGLE}" 
    rpm -ihv  http://yum-repo01/Magento/google-authenticator-1.01-1.el7.centos.x86_64.rpm
    sed -i "s/ChallengeResponseAuthentication .*/ChallengeResponseAuthentication yes/" ${SSHCONF}
    sed -i "2 iauth       required     pam_google_authenticator.so secret=/home/${USER}/.google_authenticator  nullok" /etc/pam.d/sshd
    	if ! [ "$chroot_dir" = "" ]; then
   			sed -i "2 iauth       required     pam_google_authenticator.so secret=/${chroot_dir}/home/${USER}/.google_authenticator  nullok" /etc/pam.d/sshd
    	fi
else 
    echo "Error install package "
    echo "Could not connect to repo server"
fi

}


yes_no() { 
local DEFAULT=N
b=$(tput bold)
end=$(tput sgr0)
col=$(tput setaf 6)

echo -e "$b $col  Continue: [Y|y][N|n][RETURN]: $end \c "
  read AWS
: ${AWS:=$DEFAULT}
         case $AWS in
         Y|y) echo "Continue...";
              break
           ;;
     	N|n) echo -e "Exit, Bye!!!";
         	exit 0
           ;;
     	*)echo -e "Enter [Y|y][N|n] or [RETURN] !!!";       
     	   ;;
   	esac

}


copy_lib() {
####Copy all dependence lib
DEFAULT=no
FROM=/lib64
DIR=""
LB=""
b=$(tput bold)
end=$(tput sgr0)
col=$(tput setaf 6)


while : 
     do
        echo ""
        read -p  "Enter a path chroot dir: " DIR 
        if  [ "$DIR" = "" ];then
           echo ""
           echo " You have to specify the path chroot directory " 
           yes_no
	else 
            if ! [ -d  "$DIR" ];then 
                 echo ""
                 echo "Could not find chroot directory"
                 echo "To return to specify the directory"
                 continue
            fi
          echo "Chroot directory : $DIR "  
          yes_no
     	fi
    done

while : 
     do 
       echo ""
       read -p "Enter the commands to copy into the $DIR: " LB
       if [ "LB" = "" ];then
           echo ""
           echo "You have to write commands for copy to chroot"
           continue
       else
           array=($LB)
           echo "Copy commands : ${array[@]}"
           echo ""
           yes_no
       fi

    done


echo""


echo -e "Start copy lib...... " 
for lb in "${array[@]}";
   do
      lb=`which $lb`
      lbdir=`echo $DIR${lb} | sed 's/\/\w*$//'`
	  echo ""
      echo $lb
	  echo "-----------"
      echo ""
      echo $lbdir
	  echo "-----------"
       	if ! [ -d $lbdir ]
          then 
            	mkdir -p $lbdir
       	fi
      cp -pn $lb $DIR${lb}  
       
     echo -e "lib copy $lb...\n" 
	 for l in $(ldd $lb | awk '{ print $1 }'); 
		do 
   			if [[ "${l:0:1}" == "/" ]];then 
       			echo "$l => $DIR${FROM}"
       			cp -pn $l  $DIR${FROM}
    		else 
       			echo "$FROM/${l} => $DIR${FROM}"
       			cp -pn $FROM/${l} $DIR${FROM}
   			fi 
	done 
done
}



#Create user in chroot 

menu_user() 
{
cat << EOF
        1.Enter login name or names  => [ andrew || andrew pete rhul ]

        2.Enter group name for login => [ developers ]

        3.Enter chroot directory     => [ chrootdev ]
        ===============================================================
EOF
}

check_env(){
cat << EOF
			1.Next Login[s] will add :	${_USER[@]}
			2.Group for login[s]	 :	${_GROUP}
			3.Add into chroot	:	${CHROOTDIR}
		===============================================================	
EOF
}

#Create user 
user() {
local COL="\\e[33;1m"
local END="\\e[0m"
local DEFAULT=N
local _USER=""
local _GROUP=""
local CHROOTDIR=""
local COUNT=0


while [ -z "$_USER" -o -z "$_GROUP" -o -z "$CHROOTDIR" ];
     do
		if [ $COUNT -gt 0 ];then
			clear
		   	menu_user
        	fi
            
		read  -t 60 -p "Enter an user or users login: " NEWUSER
		_USER=($NEWUSER)
       		echo					
        	read  -n 20 -t 60 -p  "Enter group for login: "  NEWGROUP
		_GROUP=$NEWGROUP
        	echo
		read  -n 20 -t 60 -p  "Enter chroot directory: "  NEWCHROOT
		CHROOTDIR=$NEWCHROOT
		echo 
		COUNT=$((COUNT + 1))
	done

echo "Check variables :"
check_env
echo -e "${COL} Continue: [Y|y][N|n][RETURN]: ${END} \c"
read -t 60 ANSWER
ANSWER=${ANSWER:=$DEFAULT}

case $ANSWER in
        Y|y) echo -e "\nStart create user"
         ;;
        N|n) echo -e "\nStop\n Exit, Bey!!!" 
             exit 0
         ;;
         *) echo "${COL} Continue: [Y|y][N|n][RETURN]: ${END}"
         ;;
esac


 

for login in "${_USER[@]}";
   do
     pass=`openssl rand -base64 12`;
     useradd -G ${_GROUP}  -m -d  ${CHROOTDIR}/home/$login $login ;
     usermod -d /home/$login  $login ;
     echo -e "$pass\n$pass" |  (passwd --stdin $login)
     chage -d 0 $login;
     sleep 1;
     echo "$login => $pass"  
  done |  egrep  "="



echo 
echo "Copy /etc/passwd  to $CHROOTDIR/etc/ ..."
cp -rp /etc/passwd  ${CHROOTDIR}/etc/passwd
echo 
echo "Copy /etc/group to $CHROOTDIR/etc/ ..."
cp -rp /etc/group  ${CHROOTDIR}/etc/group

}

# Main Menu

main_menu()
{
clear
cat << EOF 
          
			 Menu for Administration
        
        1) Install chroot environment
        2) Install google authenticator rpm
        3) Add command and lib to chroot
        4) Create user
        5) quit 
        ====================================


EOF
echo ""
read -n 1 -p "Select option > " option
}

while true; do

main_menu
case $option in
            1) echo -e "\n\tCreate chroot environment"
               chroot_env
              ;;
            2) echo -e "\n\tInstall google-authenticator rpm package"
               install_rpm
              ;;
            3) echo -e "\n\tCopy command and lib to chroot directory"
              copy_lib
              ;;
            4) echo "Add user to chroot environment"
               user
              ;;
            5|q) echo -e "\n\tExit from menu!" ; break
              ;;
            *) echo -e "\n\t Select one of menu options"
            esac
            echo ""
            echo -e "\t Please push Enter and Select options from menu: "
			read option
done
